#!/bin/bash
download_and_make(){
	echo "Checking out $BRANCH"
	git clone -b "$BRANCH" https://gitlab.com/mrvikclase/arribes.net.git arribes_git||exit 100
	cd arribes_git&&bash deploy.sh tarball true&&cd ..&&rm -rf arribes_git
}
decompress_in_www(){
	stat www&>/dev/null&&rm -rf www
	mkdir www
	tar -xf "$1" -C www/
}
export BRANCH="master"
if [ "$2" != "" ]; then
    export BRANCH="$2"
fi

if [ "$1" == "update" ]; then
	download_and_make
	decompress_in_www "arribes_git.tar"
fi
if [ "$1" == "tarball" ]; then
	decompress_in_www "../app1.tar"
fi
if [ "$1" == "update-tarball" ]; then
	stat www 2>/dev/null&&rm -rf www
	mkdir www
	echo "Download artifacts for $BRANCH"
	wget -O www.tar https://gitlab.com/mrvikclase/arribes.net/-/jobs/artifacts/$BRANCH/raw/arribes.net.tar\?job\=Prepare_tarball ||exit 1
	tar -C www -xf www.tar
	rm www.tar
fi
